#kisso app 端 api 验证演示 demo 

kisso  =  cookie sso 基于 Cookie 的 SSO 中间件，它是一把快速开发 java Web 登录系统（SSO）的瑞士军刀。


[kisso 帮助文档下载](http://git.oschina.net/baomidou/kisso/attach_files)


> 技术讨论 QQ 群 235079513 

[kisso 源码地址](http://git.oschina.net/baomidou/kisso)

[kisso 客户端 app 不同语言加密源码 C# C++ Java php Python](http://git.oschina.net/baomidou/kisso/attach_files)

http://www.oschina.net/p/kisso


# 执行 com.baomidou.kisso.apiclient 方法

输出内容：

 getApiToken json:{"code":"200","data":"{\"accessToken\":\"w5pAASZ8dmlFbasOwMbbQgTUXjfzjebwhMQzTRUVLns.\",\"aesKey\":\"Er7B95UQ90hC1lKVI009o0i7y82Vg8buf91SX407g94\",\"token\":\"6344zS237l\"}","msg":"ok"}

 access_token: w5pAASZ8dmlFbasOwMbbQgTUXjfzjebwhMQzTRUVLns.

 getTestUserById json:{"password":"123","userId":10001,"userName":"test"}

 测试API userName: test


Usage
===========
[kisso 依赖 jars](http://git.oschina.net/baomidou/kisso/wikis/kisso-%E4%BE%9D%E8%B5%96%E5%8C%85-jars)

[kisso_Jfinal 演示 demo](http://git.oschina.net/juapk/kisso_jfinal)

[kisso_SpringMvc 演示 demo](http://git.oschina.net/juapk/kisso_springmvc)

[kisso_crossdomain 跨域演示 demo](http://git.oschina.net/juapk/kisso_crossdomain)

捐赠 kisso
====================

![捐赠 kisso](http://git.oschina.net/uploads/images/2015/1222/211207_0acab44e_12260.png "支持一下kisso")


关注我
====================
![程序员日记](http://git.oschina.net/uploads/images/2016/0121/093728_1bc1658f_12260.png "程序员日记")
